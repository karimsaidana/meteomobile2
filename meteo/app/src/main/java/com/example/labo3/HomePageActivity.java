package com.example.labo3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class HomePageActivity extends AppCompatActivity {
    private String username,password;
    private TextView success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home_page);
        Intent intent = getIntent();
        this.username = intent.getExtras().getString("username");
        this.password = intent.getExtras().getString("password");
        success = findViewById(R.id.SuccessInfo);
        success.setText("Hello, "+username);
        Button clickButton = (Button) findViewById(R.id.register_button);
        clickButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sensors s = new Sensors(HomePageActivity.this);

            }
        });

    }
    public void directTemp(View view)
    {
        Intent i = new Intent(HomePageActivity.this, Temperature.class);
        startActivity(i);
    }
    public void directHumi(View view)
    {
        Intent i = new Intent(HomePageActivity.this, humidity.class);
        startActivity(i);
    }
    public void directPres(View view)
    {
        Intent i = new Intent(HomePageActivity.this, pressure.class);
        startActivity(i);
    }
    public void directLumi(View view)
    {
        Intent i = new Intent(HomePageActivity.this, luminosity.class);
        startActivity(i);
    }
}

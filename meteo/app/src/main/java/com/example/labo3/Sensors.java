package com.example.labo3;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventCallback;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.widget.Toast;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class Sensors implements SensorEventListener {

    private List<Sensor> sensorList;
    private Context context;
    private SensorManager sm;
    private Sensor temperature;
    private Sensor pressure;
    private Sensor humidity;
    private Sensor luminosity;
    private float temp, pres, humi, lumi;
    private GPSLocationTracker gps;
    private Location location;

    public Sensors(Context context) {
        this.context = context;
        this.sm = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
        this.sensorList = sm.getSensorList(Sensor.TYPE_ALL);
        this.temperature = sm.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        this.pressure = sm.getDefaultSensor(Sensor.TYPE_PRESSURE);
        this.humidity = sm.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        this.luminosity = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
        sm.registerListener((SensorEventListener) this, luminosity, 11);
        sm.registerListener((SensorEventListener) this, temperature, 11);
        sm.registerListener((SensorEventListener) this, humidity, 11);
        sm.registerListener((SensorEventListener) this, pressure, 11);

        final Geocoder geocoder = new Geocoder(this.context);
        gps = new GPSLocationTracker(context);
        if(gps.canGetLocation)
        {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            String country="NAN";
            String province="NAN";
            String city="NAN";
            List<Address> addressList = null;
            try
            {
                addressList = geocoder.getFromLocation(latitude,longitude,1);
                country= addressList.get(0).getCountryName();
                province= addressList.get(0).getAdminArea();
                city= addressList.get(0).getLocality();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }


            this.location = new Location(country,province,city);
        }
        else
        {
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            this.lumi = event.values[0];
        } else if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
            this.pres = event.values[0];
        } else if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
            this.temp = event.values[0];
        } else if (event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY) {
            this.humi = event.values[0];
        }
        getInfo();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void getInfo() {
        Date date = new Date();
        if (pres != 0 && temp != 0 && lumi != 0 && humi != 0) {
             Weather w = new Weather(this.temp, this.pres, this.humi, this.lumi, location, date);
            Toast toast = Toast.makeText(context,
                    "The following information will be automatically inserted in our database: "+"\nHumidity: " + w.getHumidity() + " %\nLuminosity: " + w.getLuminosity() + " lx\nPressure: " + w.getPressure() + " mbar\nTemperature: " + w.getTemperature() + " °C\nLocation: " +w.getLocation().getCity()+", "+w.getLocation().getProvince()+", "+w.getLocation().getCountry()+"\nDate: " + w.getDate(),
                    Toast.LENGTH_LONG);
            toast.show();
            sm.unregisterListener(this);
        }
    }
}

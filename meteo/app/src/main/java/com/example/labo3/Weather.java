package com.example.labo3;

import java.util.Date;

public class Weather {
    float pressure,humidity,temperature,luminosity;
    Location location;
    Date date;
    public Weather(float temperature,float pressure,float humidity,float luminosity,Location location,Date date)
    {
        this.humidity=humidity;
        this.temperature=temperature;
        this.luminosity=luminosity;
        this.pressure=pressure;
        this.location=location;
        this.date=date;
    }
    public float getPressure()
    {
        return this.pressure;
    }
    public float getHumidity()
    {
        return this.humidity;
    }
    public float getTemperature()
    {
        return this.temperature;
    }
    public float getLuminosity()
    {
        return this.luminosity;
    }
    public Location getLocation()
    {
        return this.location;
    }
    public Date getDate()
    {
        return this.date;
    }
}

package com.example.labo3;

import android.content.ContentValues;
import android.content.Context;
import android.database.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MyDBAdapter {
    private Context context;
    private String DATABASE_NAME="labo3.db";
    private MyDBHelper dbHelper;
    private int DATABASE_VERSION=1;
    private SQLiteDatabase mySQLiteDatabase;
    public MyDBAdapter(Context context)
    {
        this.context=context;
        dbHelper=new MyDBHelper(context,DATABASE_NAME,null,DATABASE_VERSION);
        //dbHelper.onCreate(mySQLiteDatabase);
    }
    public void open()
    {
        this.mySQLiteDatabase=dbHelper.getWritableDatabase();
    }
    public Credentials login(String username,String password)
    {
        Cursor cursor = mySQLiteDatabase.rawQuery("SELECT * FROM Login WHERE Username = '"+username+"' AND Password= '"+password+"' ", null);
        Credentials user = new Credentials();
        if(cursor !=null && cursor.moveToFirst())
            user = new Credentials(cursor.getString(1), cursor.getString(2));
        return user;
    }

}
class MyDBHelper extends SQLiteOpenHelper
{

    public MyDBHelper(Context context, String dbname, SQLiteDatabase.CursorFactory factory, int version) {
        super(context,dbname,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE Login(id integer primary key AUTOINCREMENT ,Username text,Password text);";
        db.execSQL(query);
        String query2 = "INSERT INTO Login(Username, Password) VALUES ('karim','123'),('malek','123'),('fernand','123'),('admin','123');";
        db.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query ="DROP TABLE IF EXISTS Login;";
        db.execSQL(query);
        onCreate(db);
    }
    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);
    }
}
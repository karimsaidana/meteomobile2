package com.example.labo3;

public class Location {
    String country,province,city;
    public Location(String country,String province,String city)
    {
        this.country=country;
        this.city=city;
        this.province=province;
    }
    public String getCountry()
    {
        return this.country;
    }
    public String getProvince()
    {
        return this.province;
    }
    public String getCity()
    {
        return this.city;
    }
}
